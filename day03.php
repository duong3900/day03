<?php
echo"<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>
    <style>
        .container {
            width: 600px;
            height: 330px;
            border: solid #006ccb 2px;
            margin-left: 30%;
            margin-top: 30px;
        }

        .container-margin {
            margin-top: 50px;
            margin-left: 25px;
        }

        .custom-table {
            border-spacing: 25px;
            border-spacing: 15px;
        }

        .display {
            width: 150px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .display2 {
            width: 230px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }
        
        .checkbox {
            font-size: 25px;

        }

        .checkbox1 {
            width: 30px;
            height: 18px;
            background-color: aqua;
            
            
        }

        .selectbox {
            width: 175px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;

        }

        #signup {
            margin-left: 30%;

        }

        .customsignup {
            color: white;
            background-color: rgb(0, 162, 40);
            height: 50px;
            width: 140px;
            border: #006ccb;
            border-radius: 10px;
            font-size: 20px;
        }
    </style>
</head>

<body>
    <div class='container'>
        <div class='container-margin'>
            <div style=' margin-left: 20px; width: 450px;'>
            </div>

            <table class='custom-table'>
                <tr>
                    <td class='display' style='background-color: #5c96c9;'>Họ và tên</td>
                    <td><input class='display2' type='text'></td>
                </tr>

                <tr>
                    <td class='display' style='background-color: #5c96c9;'>Giới tính</td>
                    
                    <td>";
                        $gioitinh = array(0 => 'Nam', 1 => 'Nữ');
                        for ($i = 0; $i <= 1 ; $i++) {
                        echo
                        "<input class='checkbox1' type='radio' id='".$i."' class='gender' name='gender' value='".$gioitinh[$i]."'/> ".$gioitinh[$i];
                        }
                    echo
                    "</td>

                </tr>

                <tr>
                    <td class='display' style='background-color: #5c96c9;'>Phân khoa</td>
                    <td><select class='selectbox'>";
                            $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($khoa as $key => $value) {
                            echo "
                            <option value='" . $key . "'>" . $value . "</option>";
                            }
                            echo "
                        </select></td>

                </tr>

            </table>

            <div id='signup'>
                <button class='customsignup'>Đăng ký</button>
            </div>
        </div>
</body>

</html>";